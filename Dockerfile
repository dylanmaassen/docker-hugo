FROM alpine:3.7
MAINTAINER Dylan Maassen van den Brink <dylan@dylanmaassen.nl>

ARG HUGO_VERSION=0.47.1
ENV HUGO_BINARY hugo_extended_${HUGO_VERSION}_linux-64bit

RUN apk update && apk add bash git build-base libc6-compat libxml2-dev libxslt-dev rsync

RUN mkdir /usr/local/hugo
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY}.tar.gz /usr/local/hugo/
RUN tar xzf /usr/local/hugo/${HUGO_BINARY}.tar.gz -C /usr/local/hugo/ \
	&& ln -s /usr/local/hugo/hugo /usr/local/bin/hugo \
	&& rm /usr/local/hugo/${HUGO_BINARY}.tar.gz \
    && ls /usr/local/hugo

CMD hugo version