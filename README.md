## This is an Dockerimage for Hugo
There are many like it but this one is mine.

Any update to this repository will be to bump the hugo version or to add some new dependencies.  
I simply wanted a container that would just get the latest version (as soon as I've updated the image, of course.) for use in Bitbucket Pipelines. It also includes rsync to push the files to my server.

Some hints about the CGO dependencies for Hugo Extended have been taken from https://github.com/felicianotech/docker-hugo but I didn't particularly want to include Ruby.

Feel free to pull this by using  
```docker pull dylanmaassen/docker-hugo```

Or, if you need SASS support and all that (I'm not including postcss-cli)  
```docker pull dylanmaassen/docker-hugo:extended```